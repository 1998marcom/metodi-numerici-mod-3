#include "path.cpp"
#include <random>
#include <cmath>
#include <iostream>
#include <map>
#include <ctime>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <tclap/CmdLine.h>
#include <fmt/format.h>
#include <vector>

typedef unsigned int uint;
using std::cout, std::endl;
using namespace std;

template <typename T> int sgn(T val) {
	    return (T(0) < val) - (val < T(0));
}
		
int main(int argc, char* argv[]) {
	
	// argv = [ env_dim, n, extra_occupation, model_scale, sim_time, beta, outfile_name ]
	// model_size = vedi giù
	try {
		TCLAP::CmdLine parser("Programma per simulare un path integral su una doppia buca. Vedi docs per altre info.");
		TCLAP::MultiSwitchArg ARG_q(
                /* flag */ "q",
                /* name */ "quiet",
                /* desc */ "Ask me to be more silent.");
        parser.add(ARG_q);
		TCLAP::ValueArg<double> ARG_a(
				/* flag */ "a",
				/* name */ "2th-coefficient",
				/* desc */ "Parameter to use as coefficient of the x^2 term of the potential.",
				/* req  */ false,
				/* defau*/ 1,
				/* typed*/ "A floating point value");
		parser.add(ARG_a);
		TCLAP::ValueArg<double> ARG_g(
				/* flag */ "g",
				/* name */ "4th-coefficient",
				/* desc */ "Parameter to use as coefficient of the x^4 term of the potential.",
				/* req  */ false,
				/* defau*/ 1,
				/* typed*/ "A floating point value");
		parser.add(ARG_g);
		TCLAP::ValueArg<std::string> ARG_sim_time(
				/* flag */ "t",
				/* name */ "time",
				/* desc */ "Steps of simulation to perform: each step involves a dense Metropolis-Hastings (with settable flipping probability). \
Each 10 steps the simulation variables are recorded. Each 100 steps variables are printed on stdout.",
				/* req  */ true,
				/* defau*/ "101",
				/* typed*/ "An unsigned integer");
		parser.add(ARG_sim_time);
		TCLAP::ValueArg<double> ARG_pflip(
				/* flag */ "p",
				/* name */ "pflip",
				/* desc */ "Base probability of flipping for the dense Metropolis-Hastings.",
				/* req  */ false,
				/* defau*/ 0.2,
				/* typed*/ "A floating point value");
		parser.add(ARG_pflip);
		TCLAP::ValueArg<double> ARG_beta(
				/* flag */ "b",
				/* name */ "beta",
				/* desc */ "Euclidean path integral beta parameter.",
				/* req  */ true,
				/* defau*/ 0.1,
				/* typed*/ "A floating point value");
		parser.add(ARG_beta);
		TCLAP::ValueArg<double> ARG_stddev(
				/* flag */ "d",
				/* name */ "stddev",
				/* desc */ "Standard deviation to use when altering a single point of the path during a MH step.",
				/* req  */ false,
				/* defau*/ 0.5,
				/* typed*/ "A floating point value");
		parser.add(ARG_stddev);
		TCLAP::ValueArg<std::string> ARG_fragments(
				/* flag */ "f",
				/* name */ "fragments",
				/* desc */ "How many time-fragments do you want to divide beta between?",
				/* req  */ false,
				/* defau*/ "20",
				/* typed*/ "An unsigned integer.");
		parser.add(ARG_fragments);
		TCLAP::ValueArg<std::string> ARG_seed(
				/* flag */ "s",
				/* name */ "seed",
				/* desc */ "Use this string as a seed for the pseudo random number generator.",
				/* req  */ false,
				/* defau*/ "CUSUMANO",
				/* typed*/ "A string");
		parser.add(ARG_seed);
		TCLAP::ValueArg<std::string> ARG_outfile(
				/* flag */ "o",
				/* name */ "outfile",
				/* desc */ "Where to output the simulation results",
				/* req  */ false,
				/* defau*/ "NULL",
				/* typed*/ "A relative path");
		parser.add(ARG_outfile);

		parser.parse(argc, argv);

		unsigned long long int sim_time = stoll(ARG_sim_time.getValue());
		double beta = ARG_beta.getValue();
		unsigned long long int fragments = stoll(ARG_fragments.getValue());
		double stddev = ARG_stddev.getValue();
		double a = ARG_a.getValue();
		double g = ARG_g.getValue();
		double pflip = ARG_pflip.getValue();
		std::string seed = ARG_seed.getValue();

		auto quietancy = ARG_q.getValue();

		double betaf = beta / fragments;

		// Building the model
		PathModel model(fragments, seed);

		// Initializing a non-thermalized state
		FOR(i, fragments) {
			model.cpu_model_str.positions[i] = 0;
		}
		model.refresh_positions();

		model.betaf = betaf;
		model.stddev = stddev;
		model.a = a;
		model.g = g;
		
		std::string outfile_name = ARG_outfile.getValue();
		std::fstream outfile_header;
		std::fstream outfile;
		if (outfile_name != "NULL") {
			outfile_header.open(outfile_name+"_header", std::fstream::out | std::fstream::trunc);
			outfile_header << "f\tt\tb\ta\tg\tp\td\ts" << endl;
			outfile_header << fragments << " ";
			outfile_header << sim_time << " ";
			outfile_header << beta << " ";
			outfile_header << a << " ";
			outfile_header << g << " ";
			outfile_header << pflip << " ";
			outfile_header << stddev << " ";
			outfile_header << seed << " ";
			outfile_header.close();

			outfile.open(outfile_name, std::fstream::in | std::fstream::out | std::fstream::trunc);
			outfile << "#Energy" << "\n";
		}

		// Simulating
		FOR(t, sim_time) {
			if (t%10 == 0) {
				if (outfile_name != "NULL") {
					outfile << model << "\n";
				}
			}
			if (not (t%100)) {
				if (not quietancy) {
                    cout << "@t=" << t << "\n";
                    auto qtys = model.get_quantities();
					cout << fmt::format("\tModel\ttotal_energy: {:03.15f}\n", qtys[0]);
					auto post = model.get_positions();
					cout << "\t";
					FOR(i, fragments) {
						cout << post[i] << " ";
					}
					cout << endl;
                } else {
                    if ( not ( t % (10* ((unsigned long long int) (pow(10,quietancy)+0.5))) ) ) {
                        auto en = model.get_energy();
                        cout << fmt::format("{:08d}\t{:03.8f}\n", t, en);
                    }
                }

			}
			model.step_metro_dense(pflip);
		}
		outfile.close();
		
		model.quit();
	} catch (TCLAP::ArgException &e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}
	return 0;
}
