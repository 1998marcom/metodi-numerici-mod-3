#ifndef FRAC_ISING_COMMON
#define FRAC_ISING_COMMON

#include <hip/hip_runtime.h>
#include <cstdint>

//#define GPU_COMPUTING

#ifdef GPU_COMPUTING

// We are on GPU
#define THREADS_PER_BLOCK 64
#define N_BLOCKS 128
	// note that N_BLOCKS < 200 in order for the MTGP32 to work on Nvidia platform

#else

// We are on CPU
#define THREADS_PER_BLOCK 256

#include <thread>
const auto N_BLOCKS = std::thread::hardware_concurrency();


#endif


#define FOR(i, N) for(unsigned long long int i=0; i<N; i++)
#define CEIL_DIV(a, b) (a-1)/b+1
// Note that a>0 for CEIL_DIV to work as expected
typedef double flt;

typedef struct{
	unsigned long long int size; // For the case of path integral this is the time length (or beta) of the path.
	flt* positions;
	flt* new_positions;
} ModelStruct;

#endif
