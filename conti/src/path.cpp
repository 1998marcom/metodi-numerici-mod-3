#ifndef FRAC_ISING_MH
#define FRAC_ISING_MH

#include <iostream>
#include <cstdlib>
#include <random>
#include <fstream>
#include <ctime>
#include <set>
#include <map>
using std::cout, std::endl;


#include <hip/hip_runtime.h>
#include "common.cpp"
#include "mt.cpp"

#ifndef GPU_COMPUTING
#include <cmath>
#endif

#define GRID_FOR(ix, N) for(unsigned long long int ix=threadIdx.x+blockIdx.x*blockDim.x; ix<N; ix+=gridDim.x*blockDim.x)
#define AT(i, N) ( (i < N) ? ( (i >= 0) ? i : i+N ) : ((i>2*N) ? (i+N) : (i-N)) )

__global__
void metro_dense_kernel_attempt(ModelStruct gpu_model_str, const float p_flip, const flt stddev) {
	GRID_FOR(i, gpu_model_str.size) {
		float randm = random_uniform_01();
		gpu_model_str.new_positions[i] = gpu_model_str.positions[i];
		if (randm < p_flip) {
			gpu_model_str.new_positions[i] += random_normal()*stddev;
		}
	}
}

inline float potential(flt pos, const float a, const float g) { // HAMILTONIAN HERE
	flt pos2 = pos*pos;
	return - a * pos2 + g * pos2 * pos2;
}

__forceinline__
__device__
float gpu_potential(flt pos, const float a, const float g) { // HAMILTONIAN HERE
	flt pos2 = pos*pos;
	return - a * pos2 + g * pos2 * pos2;
}

__global__
void metro_dense_kernel_finalize(ModelStruct gpu_model_str, const float betaf, const float a, const float g) {
	unsigned long long size = gpu_model_str.size;
	GRID_FOR(i, size) {
		// Conditions under which nothing occurs
		if (
			(gpu_model_str.positions[i] == gpu_model_str.new_positions[i]) or
			(i < size-1 and gpu_model_str.positions[i+1] != gpu_model_str.new_positions[i+1]) or
			(i > 0 and gpu_model_str.positions[i-1] != gpu_model_str.new_positions[i-1])
		) continue;
		//std::cout << "Not continue" <<std::endl;

		// Qua sta il Metropolis // HAMILTONIAN HERE
		/**
		float abs_deriv = 0.5 * (
				abs( gpu_model_str.positions[AT(i+1, size)] - gpu_model_str.positions[AT(i, size)] ) +
				abs( gpu_model_str.positions[AT(i, size)] - gpu_model_str.positions[AT(i-1, size)] )
				);
		*/
		float abs_deriv_1 = abs( gpu_model_str.positions[AT(i+1, size)] - gpu_model_str.positions[AT(i, size)] );
		float abs_deriv_2 =	abs( gpu_model_str.positions[AT(i, size)] - gpu_model_str.positions[AT(i-1, size)] );
		float kin_en = 0.5 * (abs_deriv_1*abs_deriv_1 + abs_deriv_2*abs_deriv_2);
		float pot_en = gpu_potential(gpu_model_str.positions[i], a, g);
		float en = kin_en/betaf + betaf*pot_en;

		/**
		float new_abs_deriv = 0.5 * (
				abs( gpu_model_str.new_positions[AT(i+1, size)] - gpu_model_str.new_positions[AT(i, size)] ) +
				abs( gpu_model_str.new_positions[AT(i, size)] - gpu_model_str.new_positions[AT(i-1, size)] )
				);
		*/
		float new_abs_deriv_1 = abs( gpu_model_str.new_positions[AT(i+1, size)] - gpu_model_str.new_positions[AT(i, size)] );
		float new_abs_deriv_2 =	abs( gpu_model_str.new_positions[AT(i, size)] - gpu_model_str.new_positions[AT(i-1, size)] );
		float new_kin_en = 0.5 * (new_abs_deriv_1*new_abs_deriv_1 + new_abs_deriv_2*new_abs_deriv_2);
		//float new_kin_en = 0.5 * new_abs_deriv*new_abs_deriv;
		float new_pot_en = gpu_potential(gpu_model_str.new_positions[i], a, g);
		float new_en = new_kin_en/betaf + betaf*new_pot_en;

		float en_delta = new_en - en;
		if (en_delta < 0) {
			gpu_model_str.positions[i] = gpu_model_str.new_positions[i];
		} else {
			float metro_p = exp(-en_delta);
			float randm = random_uniform_01();
			if (randm < metro_p) {
				gpu_model_str.positions[i] = gpu_model_str.new_positions[i];
				//std::cout << "Flip" << std::endl;
			} else {
				//std::cout << "Flip no" << std::endl;
			}
		}
	}
}

// Then we would need the Wolff kernels (TODO)

class PathModel {
	private:
		std::mt19937 cpu_random_generator;
		std::uniform_int_distribution<unsigned int> cpu_distribution_unsigned;
		std::uniform_real_distribution<float> cpu_distribution_float_01;
		ModelStruct gpu_model_str;
		bool on_gpu;
		__host__
		void to_gpu() {
			if (on_gpu) return;
			on_gpu = true;
			hipMemcpyHtoD(gpu_model_str.positions, cpu_model_str.positions, cpu_model_str.size*sizeof(flt));
		}
		__host__
		void to_cpu() {
			if (not on_gpu) return;
			on_gpu = false;
			hipMemcpyDtoH(cpu_model_str.positions, gpu_model_str.positions, cpu_model_str.size*sizeof(flt));
		}
		bool quit_flag = false;
	public:
		float betaf = 1;
		float stddev = 1;
		float a = 1;
		float g = 1;
		ModelStruct cpu_model_str; // it's public so that copying it's not needed, you can access its members and build it in-place
		PathModel(const unsigned long long int& size, std::string seed) : cpu_distribution_float_01(0, 1) {
			cpu_model_str.size = size;
			cpu_model_str.positions = malloc(size*sizeof(flt)); // Dio perdoni il malloc, ma dato che sotto devo usare hipMalloc ...
			cpu_model_str.new_positions = malloc(size*sizeof(flt));

			gpu_model_str.size = size;
			hipMalloc(& gpu_model_str.positions, size*sizeof(flt));
			hipMalloc(& gpu_model_str.new_positions, size*sizeof(flt));

			on_gpu = false;
			init_generators(seed);
			cpu_random_generator.seed(time(NULL));
		}
		void quit() {
			quit_flag = true;
		}
		~PathModel() { 
			if (quit_flag) return; // Segfault stocastico in uscita di programma perché suppongo che la memoria si liberi in async;
			free(cpu_model_str.positions); free(cpu_model_str.new_positions);
			hipFree(gpu_model_str.positions); hipFree(gpu_model_str.new_positions);
		}
		flt* get_positions() {
			to_cpu();
			return cpu_model_str.positions;
		}
		void refresh_positions() {
			on_gpu = false;
		}
		double get_energy() const { // HAMILTONIAN HERE
			to_cpu();
			double tot_en = 0;
			unsigned long long size = cpu_model_str.size;
			FOR(i, size) {
				float abs_deriv = abs( cpu_model_str.positions[AT(i+1, size)] - cpu_model_str.positions[AT(i, size)] );
				float kin_en = - 0.5 * abs_deriv*abs_deriv/(betaf*betaf);
				float pot_en = potential(cpu_model_str.positions[i], a, g);
				float en = kin_en + pot_en;
				tot_en += en;
			}
			return tot_en/size;
		}
		virtual std::vector<double> get_quantities() { // Feel free to inherit from this class and override as you wish
			std::vector<double> result;
			result.push_back(get_energy());
			FOR(i, cpu_model_str.size) {
				result.push_back(cpu_model_str.positions[i]);
			}
			return result;
		}
		__host__
		void step_metro_dense(const float& p_flip) { 
			if (not on_gpu) to_gpu();
			//std::cout << " Metro dense kernel attempt " << std::endl;
			hipLaunchKernelGGL(
					metro_dense_kernel_attempt, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, p_flip, stddev);
			hipDeviceSynchronize();
			//std::cout << " Metro dense kernel finalize " << std::endl;
			hipLaunchKernelGGL(
					metro_dense_kernel_finalize, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, betaf, a, g);
		}

		friend std::ostream& operator<< (std::ostream& stream, PathModel& ising);

};

std::ostream& operator<< (std::ostream& stream, PathModel& ising) {
	std::vector<double> quantities = ising.get_quantities();
	for(const auto& qty: quantities) {
		stream << qty << "\t";
	}
	//stream << "\n";
	return stream;
}

#endif
