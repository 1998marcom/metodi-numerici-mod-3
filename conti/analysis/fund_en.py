# Here we have three main functions
from pathlib import Path
from pprint import pprint
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from tqdm import tqdm

import simulation as sim
sim.main_exe = '../build/doble_socket'

class EnEta:
    """
    Here we must produce a plot of avg energy vs eta, with sample data, 
    and with a parabola fit so as to infer the continuum limit.
    example_kwargs = {
        'b': 40,
        't': 4000000,
        's': 'RANDOM',
        'd': 0.3,
        'a': 1, # Unless you really know what you are doing, set a=1
        'g': 0.3,
        'p': 0.15,
    }
    """
    
    def __init__(self, path):
        self.span = sim.Span(path, load=False)

    def fit_func(self, x, a, b):
        return a+x*x*b
    
    def run(self, etas, log=True, **kwargs):
        if not self.span.runned:
            beta = float(kwargs['b'])
            etas = np.array(etas)
            ff = [int(f) for f in beta/etas]
            self.span.run('f', ff, log=log, **kwargs, load=False)
            # Note that load=False make simulations load only kwargs
        #print("Example kwargs: ")
        #pprint(self.span.simulations[0].kwargs, indent=4)

    def load(self, blocksize=None, resample=100):
        nis = []; means = []; unc = []
        for simul in self.span.get_simulations():
            ni = float(simul.kwargs['b'])/float(simul.kwargs['f'])
            nis.append(ni)
            DISCARD = int(simul.kwargs['f'])*5 # Sth like 10 shoul be ok
            energy_data = simul.data['u'][DISCARD:]+1.0/2/ni
            mean = np.mean(energy_data); 
            un = sim.bootstrap(energy_data, blocksize=blocksize, 
                               resample=resample)
            means.append(mean); unc.append(un)
        assert len(nis) == len(means)
        assert len(nis) == len(unc)
        data = [
            {
                'eta': nis[i],
                'energy': means[i],
                'error': unc[i],
            }
            for i in range(len(nis))
        ]
        self.data = sorted(
            data,
            key=lambda x: x['eta'],
        )
        self.array_data = {
            'eta': np.array([d['eta'] for d in self.data]),
            'energy': np.array([d['energy'] for d in self.data]),
            'error': np.array([d['error'] for d in self.data]),
        }
    
    def fit(self, p0=None, err_mult=4):
        self.popt, self.pcov = curve_fit(
            self.fit_func, 
            self.array_data['eta'],
            self.array_data['energy'],
            p0=p0,
            sigma=self.array_data['error']*err_mult,
        )
        #print("popt, uncertainties: ", 
              #self.popt, np.sqrt(np.diag(self.pcov)))
    
    def plot(self, err_mult=4):
        plt.errorbar(
            self.array_data['eta'], 
            self.array_data['energy'], 
            self.array_data['error']*err_mult,
            fmt="none", ecolor="r", 
            label='Dati per $\\beta={}$'
                .format(self.span.simulations[0].kwargs['b']),
        )
        xx = np.linspace(0, np.max(self.array_data['eta']), 100)
        yy = self.fit_func(xx, *self.popt)
        plt.plot(xx, yy, label='Fit')
        #plt.show() #Magari poi aggiungiamo qualcosa prima di mostrare
    
    def pipeline(self, etas, p0=None, err_mult=4, 
                 blocksize=None, resample=100, **kwargs):
        """
        Calls: run, load, fit, plot
        """
        self.run(etas, **kwargs)
        self.load(blocksize=blocksize, resample=resample)
        self.fit(p0=p0, err_mult=err_mult)
        self.plot(err_mult=err_mult)
        
class EnBeta:
    """
    Here we must call en_vs_eta some times to get reasonable results for
    different values of beta, and plot the result of the continuum limit        
    vs beta
    example_kwargs = {
        'b': 40,
        't': 4000000,
        's': 'RANDOM',
        'd': 0.3,
        'a': 1, # Unless you really know what you are doing, set a=1
        'g': 0.3,
        'p': 0.15,
    }
    """
    path = ""
    runned = False
    enetas = []
    
    def __init__(self, path, key):
        self.key = key
        self.path = Path(path)
        if self.path.exists():
            self._load()
            self.runned = True
            
    def run(self, values, etas, **kwargs):
        if self.runned:
            return
        self.path.mkdir(exist_ok=False) # Tira l'eccezione e si ferma
        params = kwargs
        print("EnBeta.run tqdm", flush=True)
        for value in tqdm(values):
            params[self.key] = value
            sim = EnEta( self.path / (self.key + '_' + str(value)) )
            sim.run(etas, log=False, **params)
        self.runned = True
        self._load()
        #print("Example kwargs: ")
        #pprint(self.enospan.simulations[0].kwargs, indent=4)
    
    def _load(self):
        print("EnBeta._load tqdm", flush=True)
        self.enetas = [
            EnEta(child) 
            for child in tqdm(self.path.iterdir())
            if not (str(child).endswith('.ipynb_checkpoints'))
        ] # Assicuriamoci l'idempotenza
        print("Example kwargs: ")
        pprint(self.enetas[0].span.simulations[0].kwargs, indent=4)

    def get_enetas(self, load=True):
        print("EnBeta.get_simulations tqdm", flush=True)
        for child in tqdm(self.path.iterdir()):
            if not (str(child).endswith('.ipynb_checkpoints')):
                e = EnEta(child)
                if load:
                    e.load()
                    e.fit()
                yield e
    
    def load(self):
        key_values = []; popt0s = []; unc = []
        for eneta in self.get_enetas(load=True):
            key_value = float(eneta.span.simulations[0].kwargs[self.key])
            key_values.append(key_value)
            popt0s.append(eneta.popt[0])
            unc.append(np.sqrt(eneta.pcov[0][0]))
        assert len(key_values) == len(popt0s)
        assert len(key_values) == len(unc)
        data = [
            {
                'key_value': key_values[i],
                'popt0': popt0s[i],
                'error': unc[i],
            }
            for i in range(len(key_values))
        ]
        self.data = sorted(
            data,
            key=lambda x: x['key_value'],
        )
        self.array_data = {
            'key_value': np.array([d['key_value'] for d in self.data]),
            'popt0': np.array([d['popt0'] for d in self.data]),
            'error': np.array([d['error'] for d in self.data]),
        }
    
    def plot(self, err_mult=1):
        plt.errorbar(
            self.array_data['key_value'], 
            self.array_data['popt0'], 
            self.array_data['error']*err_mult,
            fmt="none", ecolor="r", 
            label='Risultati delle simulazioni',
        )
    
def en_vs_zeta(span):
    """
    Here we must call en_vs_eta some times to get reasonable results for
    different values of zeta, and plot the result of the continuum limit
    vs zeta
    """
    pass