# Grazie https://gitlab.com/andrea.ortone/metodi-numerici-3/-/blob/master/src/schrodinger.py
import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse
from scipy.sparse import linalg


def eigen(xmin, xmax, N, Vfun, params, neigs):
    x = np.linspace(xmin, xmax, N)
    dx = x[1] - x[0]

    H = sparse.eye(N, N, format='lil') * 2

    for i in range(N - 1):
        H[i, i + 1] = -1
        H[i + 1, i] = -1

    H /= 2*(dx ** 2)

    V = Vfun(x, *params)
    for i in range(N):
        H[i, i] += V[i]
    H = H.tocsc()

    eigenvalues, eigenvectors = linalg.eigs(H, k=neigs, which='SM')
    for i in range(neigs):
        eigenvalues = np.real(eigenvalues)

    return eigenvalues, eigenvectors

#a = 1
#g = 0.3
#e, ev = eigen(-8, 8, 2000, potential, [a, g], 2)
#print(e)