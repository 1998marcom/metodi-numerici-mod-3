# Conti del modulo 3 dell'esame di Metodi Numerici

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [Che cosa famo](#che-cosa-famo)
  - [Lagrangiana](#lagrangiana)
  - [Adimensionalizzazione](#adimensionalizzazione)
  - [Energia dello stato fondamentale](#energia-dello-stato-fondamentale)
  - [Funzione d'onda dello stato fondamentale](#funzione-donda-dello-stato-fondamentale)
- [Come lo famo](#come-lo-famo)
- [SubTasks](#subtasks)
  - [Simulare](#simulare)
  - [Analizzare](#analizzare)
- [Note che sono un abbozzo di documentazione](#note-che-sono-un-abbozzo-di-documentazione)
  - [Come compilare](#come-compilare)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Che cosa famo
Vogliamo trovare a mezzo di path integral, per una doppia buca descritta da un potenziale `V(x) = - a * x^2 + g*x^4`:
- Energia dello stato fondamentale - confrontata con il limite teorico se le buche sono molto lontane (ordine 0, niente WKB).
- Funzione d'onda dello stato fondamentale.

### Lagrangiana
L =  (dx/dt)^2 /2 - V(x) 

### Adimensionalizzazione
Usiamo in modo furbo la massa. Vedi relazione.

### Energia dello stato fondamentale
E =  (dx/dt)^2 /2 + V(x)

### Funzione d'onda dello stato fondamentale
Istogramma delle x. Poi sotto radice.

## Come lo famo
Un bellissimo path integral. In rotazione di Wick, con una beta molto grossa, dovrebbe andare bene.

## SubTasks

### Simulare
- [x] Scrivere il codice anche più schifoso che ci sia, ma purché sia veloce da scrivere. 
- [ ] Compilare il codice dopo aver scritto nel programma i funzionali che vogliamo salvare.

`g++ doble_socket.cpp -o ../build/doble_socket -std=gnu++17 -ltbb -lfmt -fopenmp -fpermissive -w -O3`

### Analizzare
- [ ] Scrivere gli script per tirare fuori le medie che ci interessano.
- [ ] Aggiustare gli script per tirare fuori le incertezze con Jackknife/bootstrap. 

Poi viene la relazione, ma è un altro paio di maniche

## Note che sono un abbozzo di documentazione

### Come compilare

Vedi [le istruzioni del modulo 1](https://gitlab.com/1998marcom/metodi-numerici-mod-1/-/tree/master/conti#come-compilare).
