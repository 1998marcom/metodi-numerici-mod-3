\documentclass[a4paper, 11pt]{article}
%\DeclareMathSizes{10}{11}{9}{8}
\usepackage[top=4cm, bottom=3cm, inner=2.8cm, outer=2.8cm]{geometry}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{wrapfig}
\hypersetup{colorlinks, linkcolor=blue}
\usepackage{float}
\newcommand{\dd}{\mathrm{d}}
\renewcommand{\tablename}{Tab.} 
\renewcommand{\figurename}{Fig.}

\title{
	Metodi Numerici per la Fisica\\
	Particella in una doppia buca con potenziale 
	quartico\footnote{\vspace{00pt}Sorgenti e pdf su 
	\url{https://gitlab.com/1998marcom/metodi-numerici-mod-3}}
	\\ \vspace{10pt}
	\small{PROGETTO MODULO 3}\\
}
\author{Marco Malandrone e Alessandro Falco}

\date{\today}

\begin{document}

\maketitle

\tableofcontents

\section{Introduzione}
\subsection{Il modello}

\subsubsection*{Potenziale}
Vogliamo studiare lo stato fondamentale di una particella di massa 
$m$ in uno spazio 1+1 dimensionale, confinata 
in un potenziale quartico $V(x) = -a x^2 + g x^4$, con $g>0, a>0$, 
affinché il potenziale non abbia instabilità 
e si abbia una situazione di doppia buca.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\textwidth]{images/potential_3_1.png}
	\caption{Potenziale con $a=1, \, g=0.3$.}
	\label{fig:label_ptential}
\end{figure}
\vspace{0pt}

\subsubsection*{Simulazione}

Per le simulazioni, utilizziamo la versione euclidea della teoria, 
descritta dalla lagrangiana:
\begin{equation}
	L_E\left(x(t), \dot x(t)\right) = \frac12m\dot x^2 + V(x) 
	= \frac12m\dot x^2 - a x^2 + g x^4
\end{equation}
Lavoriamo inoltre con quantità adimensionali, 
utilizzando la massa come unica quantità dimensionale, ponendo $\hbar = c = 1$. 
In particolare, siano 
$x= \frac1m\hat x$, $t=\frac1m\hat t$, $a = m^3\hat a$, $g=m^5\hat g$, per cui:
\begin{equation}
	L_E\left(\hat x(t), \dot{\hat x}(t)\right) = \frac12m\dot {\hat x}^2 + V(x) 
		= m\left[\frac12\dot {\hat x}^2 - \hat a \hat x^2 + \hat g \hat x^4\right]
\end{equation}
Definiamo anche $\hat\beta$ con la relazione $\beta = \frac1m\hat\beta$, 
in modo che ogni traiettoria $x(t)$ sia associata ad un fattore:

\[
	\exp\left[- \int_0^\beta \dd t\;L_E\left(x(t), \dot x(t)\right)\right] 
	= \exp\left[
		- \int_0^{\hat\beta} \frac{\dd \hat t}{m}\;L_E\left(\hat x(t), \dot {\hat x}(t)
	\right)\right] =
\]
\[
	= \exp\left[- \int_0^{\hat\beta} \dd \hat t\;\left( 
		\frac12\dot {\hat x}^2 - \hat a \hat x^2 + \hat g \hat x^4 
	\right)\right]
\]

Vogliamo ora discretizzare il tempo in $N$ punti; 
per farlo, assicuriamoci che l'integrale abbia come estremi di integrazione 0 e $N$, 
sostituendo $\hat t = \hat \tau \hat \beta / N$:

\begin{equation}
	\exp\left[-\frac{\hat\beta}{N}\sum_{n=0}^{N-1}
		\left(\frac{N^2}{2\hat\beta^2}\left(\hat x_{n+1}- \hat x_n\right)^2 - \hat a \hat x_n^2 + \hat g \hat x_n^4
	\right)\right]
\end{equation}

D'ora in avanti ci riferiremo solo alle quantità adimensionali e non useremo più simboli extra $(\,\hat{}\,)$ 
per connotare il fatto che una grandezza sia adimensionale.

Possiamo ora rilassare in realtà il vincolo $c=1$, scegliendo ad esempio $c=\sqrt[4]{a}$. In tal modo 
$x \rightarrow x/\sqrt[4]{a}$ e otteniamo:

\begin{equation}
	\exp\left[-\frac{\beta \sqrt{a}}{N}\sum_{n=0}^{N-1}
		\left(\frac{N^2}{2\beta^2a}\left(x_{n+1}- x_n\right)^2 - x_n^2 + \frac{g}{a^{3/2}} x_n^4
	\right)\right]
	\label{eq:expo_con_beta}
\end{equation}
Sarà nel seguito utile definire $\eta = \frac{\beta\sqrt{a}}{N}$ e $\zeta = g/a^{3/2}$, cosi da avere:
\begin{equation}
	\exp\left[-\eta\sum_{n=0}^{N-1}
		\left(\frac{1}{2\eta^2}\left(x_{n+1}- x_n\right)^2 - x_n^2 + \zeta x_n^4
	\right)\right]
	\label{eq:expo_con_eta}
\end{equation}



\subsection{Gli osservabili}
Abbiamo scelto di studiare l'energia dello stato fondamentale al variare dei parametri del sistema 
($\beta, \zeta, \eta$).

\subsubsection*{Energia del fondamentale}
Per calcolare l'energia dello stato fondamentale abbiamo valutato $\langle H\rangle = U(\beta)=-\partial_\beta\log Z$ 
nel limite di basse temperature $\beta\rightarrow\infty$, e nel limite del continuo $\eta \rightarrow 0$. 
Ovvero abbiamo misurato:
\begin{equation}
	U(\beta) = -\partial_\beta\log Z = 
	\left\langle \frac{\sqrt{a}}{N}\sum_{n=0}^{N-1}\left(
		-\frac{N^2}{2\beta^2a}(x_{n+1}-x_n)^2-x_n^2+\frac{g}{a^{3/2}}x_n^4
	\right)\right\rangle + \frac{N}{2\beta}
\end{equation}
o equivalentemente, sostituendo $\eta = \frac{\beta\sqrt{a}}{N}$ e $\zeta = g/a^{3/2}$:
\begin{equation}
	U(\beta) = \sqrt{a}\left\{
	\left\langle \frac{1}{N}\sum_{n=0}^{N-1}\left(
		-\frac{1}{2\eta^2}(x_{n+1}-x_n)^2-x_n^2+\zeta x_n^4
	\right)\right\rangle + \frac{1}{2\eta}
	\right\}
	\label{eq:internal_energy}
\end{equation}

Nel seguito ci limiteremo a porre quindi $a=1$ e a variare gli altri parametri,
con la consapevolezza di poter recuperare per via analitica la dipendenza di $U$
da $a$.



\section{Implementazione}
Abbiamo simulato il sistema usando una variante parallelizzata del Metropolis-Hastings
e usato come generatore di numeri pseudocasuali MT19937,
un generatore del tipo Mersenne-Twister.
La simulazione è implementata in \texttt{C++};
il codice sorgente è reperibile nel repository
\href{https://gitlab.com/1998marcom/metodi-numerici-mod-3}{metodi-numerici-mod-3}.\\

\subsection{Metropolis-Hastings \textit{parallelo}}
Abbiamo riutilizzato lo stesso algoritmo utilizzato per il progetto del modulo
1, opportunamente modificato.
\subsubsection*{L'idea}
Il normale algortimo Metropolis-Hastings consente di far evolvere un sistema 
cambiando il valore della posizione della particella un sito alla volta.
Nel caso però in cui la lagrangiana sia esprimibile come interazioni solamente fra tempi vicini,
è possibile cambiare il valore della posizione in più di un tempo euclideo alla volta,
prestando attenzione solo a variazioni temporalmente locali dell'azione.
Tutto, però, a condizione di non cambiare in un'unica \textit{operazione} (nel seguito anche detta \textit{tick})
due posizioni $x(t)$ temporalmente prime vicine (ovvero non è possibile cambiare della stessa \textit{operazione}
$x(t)$ e $x(t+1)$).
\subsubsection*{L'algoritmo}
Ciascun \textit{tick} di evoluzione dell'algoritmo può essere diviso in due step.
Fra questi due step è opportuno assicurarsi che la memoria venga sincronizzata e tutti i core condividano
la stessa memoria in lettura.
\begin{enumerate}
    \item Per ogni tempo $t$ del sistema:
        \begin{itemize}
            \item Si sceglie con probabilità $p$ se cambiare la posizione.
            \item Se si sceglie di cambiare la posizione, come nel caso di un Metropolis-Hastings normale,
				si estrae un nuovo valore: $x_1(t) = x(t) +\delta$,
                dove $x(t)$ è il vecchio valore del campo,
                $x_1(t)$ è il possibile nuovo valore del campo al tempo $t+1$
                e $\delta$ è un valore estratto con una distribuzione arbitraria.
            \item Si memorizzano $x_1(t)$ e $x(t)$ per lo step 2.
        \end{itemize}
    \item Per ogni tempo $t$ del sistema:
        \begin{itemize}
			\item Se nello step 1 si è scelto di non cambiare la posizione, assegna $x(t+1) = x(t)$.
            \item Se nello step 1 una qualunque delle posizioni temporalmente prime vicine  
				$t'$ ha memorizzato un valore
                $x_1(t')\neq x(t')$, assegna $x(t+1) = x(t)$.
            \item Se nessuna delle due precedenti condizioni è verificata, calcola la variazione dell'azione
                $\Delta S$ e assegna $x(t+1)$ con un algoritmo accept-reject:
                \[
                    x(t+1) =
                        \begin{cases}
                           	x(t) &\text{if }\Delta S>0 \text{ with probability}\exp[-\Delta S]\\
                            x_1(t) &\mathrm{otherwise}
                        \end{cases}
                \]
        \end{itemize}
\end{enumerate}

\subsubsection*{Verifica del bilancio dettagliato}
La verifica della condizione del bilancio dettagliato discende dal rispetto della condizione per il normale
Metropolis-Hastings, dato che è possibile considerare un \textit{tick} del Metropolis-Hastings parallelo come
una concatenazione di passi del normale Metropolis-Hastings in un qualunque ordine, non essendo questi passi in alcun modo dipendenti l'uno dall'altro.

\subsubsection*{Possibili miglioramenti}
Abbiamo pensato a questa variante del MH in modo da poter funzionare su un generico grafo.
In realtà questo problema è unidimensionale, e quindi è possibile semplicemente aggiornare 
in un'unica \textit{operazione} tutte le posizioni con coordinata temporale con la stessa parità modulo-2.

\section{Analisi}
\subsection{Energia del fondamentale \textit{vs} $\eta$} \label{par:e_vs_eta}
Abbiamo cominciato a valutare innanzitutto il problema del limite del continuo
$\eta\rightarrow0$. Scelti $\zeta=0.3$, $\beta=50$, abbiamo eseguito
delle simulazioni per diversi valori di $\eta$, registrando l'energia interna,
ovvero l'osservabile riportato nell'espressione-\ref{eq:internal_energy}.

Abbiamo poi utilizzato i valori di energia interna registrati per 
eseguire un \textit{best fit} dell'energia interna al variare di $\eta$,
con una funzione quadratica $U(\eta) = \xi +\chi\eta^2$, per tenere conto delle
correzioni a $\eta$ finito. Ne abbiamo quindi estrapolato il limite
$\eta\rightarrow0$. I dati utilizzati e la curva di best fit sono riportati in
figura-\ref{fig:e_vs_eta}. 

I risultati del fit indicano per il limite del continuo un valore di
$U(0)=\xi=-0.1874\pm0.0028$, con $\chi^2/\text{ndof}=1.38$.

Infine abbiamo confrontato i risultati così ottenuti con il valore dell'energia
dello stato fondamentale predetto dalla diagonalizzazione numerica
dell'Hamiltoniana, di -0.1824, riportato in arancione in
figura-\ref{fig:e_vs_eta}.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/e_vs_eta.png}
	\caption{Energia interna nel limite del continuo $\eta \rightarrow 0$, per $\zeta=0.3$, $\beta=50$}
	\label{fig:e_vs_eta}
\end{figure}
\subsection{Energia del fondamentale \textit{vs} $\beta$}
Abbiamo poi cercato di determinare a partire da quale valore di $\beta$ l'energia
interna misurata fosse compatibile, considerate le barre di errore, con il
valore di energia predetto dalla diagonalizzazione per l'energia dello stato
fondamentale. Per stimare l'energia nel limite del continuo a fissato $\beta$
abbiamo proceduto come al punto precedente, eseguendo simulazioni per diversi
valori di $\eta$ ed estrapolando con un \textit{fit} il limite
$\eta\rightarrow0$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/e_vs_beta.png}
	\caption{Energia interna nel limite di basse temperature $\beta \rightarrow \infty$, per $\zeta=0.3$}
	\label{fig:e_vs_beta}
\end{figure}

Abbiamo riportato il valore predetto dalla diagonalizzazione per l'energia
dello stato fondamentale 
e le misure ottenute dalle simulazioni in figura-\ref{fig:e_vs_beta}.

Come osservabile in figura-\ref{fig:e_vs_beta}, per $\beta \gtrsim 10$ l'energia
dello stato fondamentale è all'interno del margine di errore dell'energia
misurata.

\subsection{Energia del fondamentale \textit{vs} $\zeta$}
Infine, abbiamo valutato l'energia dello stato fondamentale al variare di
$\zeta$. Per ottenere l'energia dello stato fondamentale abbiamo fissato un
valore di $\beta = 40$ abbastanza elevato, anche sulla base di quanto mostrato
dai dati della figura-\ref{fig:e_vs_beta}, e abbiamo ripetuto la procedura
utilizzata al paragrafo \ref{par:e_vs_eta} per estrapolare il limite
$\eta\rightarrow0$. Abbiamo ripetuto questa procedura per diversi valori di
$\zeta$ e riportato i risultati ottenuti da queste simulazioni in
figura-\ref{fig:e_vs_zeta}. 

Abbiamo confrontato i risultati delle simulazioni
con l'energia dello stato fondamentale predetta dalla diagonalizzazione
dell'Hamiltoniana. Il $\chi^2$ ridotto vale $\chi^2_r = 0.58$.
\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{images/e_vs_zeta.png}
	\caption{Energia del fondamentale nel limite del continuo $\eta \rightarrow 0$, per $\zeta=0.3$}
	\label{fig:e_vs_zeta}
\end{figure}

Come intuibile dalla figura-\ref{fig:e_vs_zeta}, l'energia del fondamentale non è
limitata dal basso per questo sistema. Ciò è direttamente legato alla scelta
dell'espressione del potenziale, che è assunto avere valore nullo in 0. Un'altra
scelta valida per fissare il potenziale è data dal richiedere che i minimi del
potenziale siano anche degli zeri del potenziale, 
come nell'esempio della figura-\ref{fig:label_ptential_offset}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\textwidth]{images/potential_3_1_offset.png}
	\caption{Potenziale con $a=1, \, g=0.3$, con un offset tale che i minimi
	siano degli zeri.}
	\label{fig:label_ptential_offset}
\end{figure}

Correggendo i risultati mostrati in figura-\ref{fig:e_vs_zeta} per ottenere i
risultati che si avrebbero nel caso di un potenziale i cui minimi siano fissati
a 0, abbiamo ottenuto la figura-\ref{fig:ec_vs_zeta}.
\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{images/ec_vs_zeta.png}
	\caption{Energia del fondamentale nel limite del continuo $\eta \rightarrow 0$, per $\zeta=0.3$}
	\label{fig:ec_vs_zeta}
\end{figure}



\end{document}
